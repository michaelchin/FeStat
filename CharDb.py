import FeStatUtils as Utils
import ClassDb

class FeCharacter( object ):
	def __init__( self ):
		self.level_ = None
		self.baseClass_ = None
		self.baseStats_ = None
		self.growthRates_ = None
		self.maxStats_ = None
	def baseLevel( self ):
		return self.level_
	def baseClass( self ):
		return self.baseClass_
	def baseStats( self ):
		return self.baseStats_
	def growthRates( self ):
		return self.growthRates_
	def maxStats( self ):
		return self.maxStats_

class Kiragi( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.Archer()
		self.baseStats_ = Utils.newFeStatLine( ('7', '6', '0', '5', '6', '8', '4', '1') )
		self.growthRates_ = Utils.newFeGrowthRate( ('45', '40', '0', '45', '50', '45', '40', '15') )
		self.maxStats_ = None

class Nyx( FeCharacter ):
	def __init__( self ):
		self.level_ = 9
		self.baseClass_ = ClassDb.DarkMage()
		self.baseStats_ = Utils.newFeStatLine( ('20', '1', '12', '5', '11', '3', '4', '8') )
		self.growthRates_ = Utils.newFeGrowthRate( ('30', '5', '50', '35', '50', '20', '15', '30') )
		self.maxStats_ = Utils.newFeStatLine( (0, 0, 3, -2, 2, -1, -2, 1) )

class Selkie( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.Kitsune()
		self.baseStats_ = Utils.newFeStatLine( ('7', '4', '3', '6', '7', '10', '6', '11') )
		self.growthRates_ = Utils.newFeGrowthRate( ('35', '30', '15', '35', '55', '60', '30', '50') )
		self.maxStats_ = None

class Felicia( FeCharacter ):
	def __init__( self ):
		self.level_ = 1
		self.baseClass_ = ClassDb.Maid()
		self.baseStats_ = Utils.newFeStatLine( ('19', '5', '9', '10', '10', '12', '5', '9') )
		self.growthRates_ = Utils.newFeGrowthRate( ('40', '10', '35', '30', '40', '55', '15', '35') )
		self.maxStats_ = Utils.newFeStatLine( (0, -2, 2, 0, 1, 0, -1, 1) )

class Ophelia( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.DarkMage()
		self.baseStats_ = Utils.newFeStatLine( ('7', '3', '6', '6', '7', '12', '2', '5') )
		self.growthRates_ = Utils.newFeGrowthRate( ('45', '15', '45', '40', '45', '65', '20', '30') )
		self.maxStats_ = None

class Gunter( FeCharacter ):
	def __init__( self ):
		self.level_ = 3
		self.baseClass_ = ClassDb.GreatKnight()
		self.baseStats_ = Utils.newFeStatLine( ('24', '10', '0', '15', '8', '9', '10', '4') )
		self.growthRates_ = Utils.newFeGrowthRate( ('15', '5', '0', '5', '0', '15', '5', '5') )
		self.maxStats_ = Utils.newFeStatLine( (0, 2, 0, 1, -2, 0, 2, -2) )

class Peri( FeCharacter ):
	def __init__( self ):
		self.level_ = 16
		self.baseClass_ = ClassDb.Cavalier()
		self.baseStats_ = Utils.newFeStatLine( ('27', '16', '0', '10', '15', '12', '12', '11') )
		self.growthRates_ = Utils.newFeGrowthRate( ('30', '50', '5', '30', '50', '35', '25', '45') )
		self.maxStats_ = Utils.newFeStatLine( (0, 1, 0, -1, 1, 0, -2, 2) )

class Rinkah( FeCharacter ):
	def __init__( self ):
		self.level_ = 4
		self.baseClass_ = ClassDb.OniSavage()
		self.baseStats_ = Utils.newFeStatLine( ('20', '8', '2', '6', '8', '5', '10', '3') )
		self.growthRates_ = Utils.newFeGrowthRate( ('20', '25', '15', '50', '45', '35', '45', '20') )
		self.maxStats_ = Utils.newFeStatLine( (0, -1, 0, -2, 1, 0, 2, 0) )

class Fuga( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.MasterOfArms()
		self.baseStats_ = Utils.newFeStatLine( ('41', '29', '0', '27', '25', '18', '29', '15') )
		self.maxStats_ = None

class Camilla( FeCharacter ):
	def __init__( self ):
		self.level_ = 1
		self.baseClass_ = ClassDb.MaligKnight()
		self.baseStats_ = Utils.newFeStatLine( ('30', '19', '11', '15', '19', '12', '18', '15') )
		self.growthRates_ = Utils.newFeGrowthRate( ('40', '50', '25', '50', '55', '25', '35', '45') )
		self.maxStats_ = Utils.newFeStatLine( (0, 1, -1, 1, 1, -2, 1, 0) )

class Sakura( FeCharacter ):
	def __init__( self ):
		self.level_ = 4
		self.baseClass_ = ClassDb.ShrineMaiden()
		self.baseStats_ = Utils.newFeStatLine( ('19', '4', '8', '6', '10', '11', '7', '8') )
		self.growthRates_ = Utils.newFeGrowthRate( ('45', '30', '50', '40', '40', '55', '30', '20') )
		self.maxStats_ = Utils.newFeStatLine( (0, 0, 2, -1, 1, 0, -1, 0) )

class Mozu( FeCharacter ):
	def __init__( self ):
		self.level_ = 1
		self.baseClass_ = ClassDb.Villager()
		self.baseStats_ = Utils.newFeStatLine( ('16', '6', '0', '5', '7', '3', '4', '1') )
		self.growthRates_ = Utils.newFeGrowthRate( ('30', '40', '5', '50', '55', '45', '35', '30') )
		self.maxStats_ = Utils.newFeStatLine( (0, 0, 0, 1, 1, 1, 0, -2) )

class Shura( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.Adventurer()
		self.baseStats_ = Utils.newFeStatLine( ('34', '20', '11', '23', '27', '15', '14', '24') )
		self.growthRates_ = Utils.newFeGrowthRate( ('30', '25', '10', '20', '35', '30', '15', '35') )
		self.maxStats_ = Utils.newFeStatLine( (0, -1, 0, -1, 3, -1, -2, 2) )

class Effie( FeCharacter ):
	def __init__( self ):
		self.level_ = 8
		self.baseClass_ = ClassDb.Knight()
		self.baseStats_ = Utils.newFeStatLine( ('24', '14', '0', '9', '5', '11', '13', '4') )
		self.growthRates_ = Utils.newFeGrowthRate( ('35', '60', '0', '35', '50', '50', '35', '30') )
		self.maxStats_ = Utils.newFeStatLine( (0, 3, 0, -1, 1, 0, -1, -1) )

class Kana( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.NohrPrince()
		self.baseStats_ = Utils.newFeStatLine( ('7', '3', '6', '8', '8', '9', '5', '5') )
		self.growthRates_ = Utils.newFeGrowthRate( ('30', '35', '30', '40', '45', '45', '25', '25') )
		self.maxStats_ = None

class Shiro( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.SpearFighter()
		self.baseStats_ = Utils.newFeStatLine( ('8', '7', '0', '5', '3', '6', '8', '5') )
		self.growthRates_ = Utils.newFeGrowthRate( ('50', '50', '0', '40', '35', '35', '45', '30') )
		self.maxStats_ = None

class Jakob( FeCharacter ):
	def __init__( self ):
		self.level_ = 1
		self.baseClass_ = ClassDb.Butler()
		self.baseStats_ = Utils.newFeStatLine( ('21', '8', '6', '12', '9', '10', '7', '6') )
		self.growthRates_ = Utils.newFeGrowthRate( ('50', '30', '15', '40', '35', '45', '25', '25') )
		self.maxStats_ = Utils.newFeStatLine( (0, 2, -2, 2, 0, -1, 0, -1) )

class Rhajat( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.Diviner()
		self.baseStats_ = Utils.newFeStatLine( ('8', '1', '10', '0', '7', '6', '5', '12') )
		self.growthRates_ = Utils.newFeGrowthRate( ('40', '15', '60', '10', '50', '30', '25', '35') )
		self.maxStats_ = None

class Soleil( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.Mercenary()
		self.baseStats_ = Utils.newFeStatLine( ('6', '7', '1', '3', '6', '7', '5', '6') )
		self.growthRates_ = Utils.newFeGrowthRate( ('25', '60', '0', '35', '35', '45', '35', '40') )
		self.maxStats_ = None

class Caeldori( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.SkyKnight()
		self.baseStats_ = Utils.newFeStatLine( ('8', '8', '3', '5', '6', '9', '5', '6') )
		self.growthRates_ = Utils.newFeGrowthRate( ('55', '35', '15', '40', '40', '45', '35', '20') )
		self.maxStats_ = None

class Xander( FeCharacter ):
	def __init__( self ):
		self.level_ = 4
		self.baseClass_ = ClassDb.Paladin()
		self.baseStats_ = Utils.newFeStatLine( ('38', '23', '4', '18', '15', '20', '23', '11') )
		self.growthRates_ = Utils.newFeGrowthRate( ('45', '50', '5', '40', '35', '60', '40', '15') )
		self.maxStats_ = Utils.newFeStatLine( (0, 2, -1, -1, -1, 2, 1, -2) )

class Silas( FeCharacter ):
	def __init__( self ):
		self.level_ = 18
		self.baseClass_ = ClassDb.Cavalier()
		self.baseStats_ = Utils.newFeStatLine( ('39', '19', '3', '16', '12', '11', '19', '14') )
		self.growthRates_ = Utils.newFeGrowthRate( ('40', '45', '5', '50', '40', '40', '40', '25') )
		self.maxStats_ = Utils.newFeStatLine( (0, 1, 0, 2, 0, -1, 0, -1) )

class Hana( FeCharacter ):
	def __init__( self ):
		self.level_ = 4
		self.baseClass_ = ClassDb.Samurai()
		self.baseStats_ = Utils.newFeStatLine( ('20', '7', '0', '9', '11', '5', '6', '9') )
		self.growthRates_ = Utils.newFeGrowthRate( ('25', '55', '10', '45', '55', '25', '20', '30') )
		self.maxStats_ = Utils.newFeStatLine( (0, 1, 0, 1, 2, -1, -3, 1) )

class Azama( FeCharacter ):
	def __init__( self ):
		self.level_ = 13
		self.baseClass_ = ClassDb.Monk()
		self.baseStats_ = Utils.newFeStatLine( ('28', '10', '8', '11', '13', '16', '14', '10') )
		self.growthRates_ = Utils.newFeGrowthRate( ('55', '50', '20', '40', '45', '40', '40', '20') )
		self.maxStats_ = Utils.newFeStatLine( (0, 2, -3, 0, 1, 0, 1, 0) )

class Leo( FeCharacter ):
	def __init__( self ):
		self.level_ = 2
		self.baseClass_ = ClassDb.DarkKnight()
		self.baseStats_ = Utils.newFeStatLine( ('34', '14', '20', '14', '15', '15', '16', '20') )
		self.growthRates_ = Utils.newFeGrowthRate( ('45', '25', '55', '35', '45', '45', '30', '45') )
		self.maxStats_ = Utils.newFeStatLine( (0, -2, 2, 0, -2, 0, 0, 2) )

class Kaden( FeCharacter ):
	def __init__( self ):
		self.level_ = 14
		self.baseClass_ = ClassDb.Kitsune()
		self.baseStats_ = Utils.newFeStatLine( ('30', '15', '1', '12', '19', '14', '9', '14') )
		self.growthRates_ = Utils.newFeGrowthRate( ('45', '40', '10', '25', '45', '50', '35', '40') )
		self.maxStats_ = Utils.newFeStatLine( (0, 1, 0, -3, 2, 1, -2, 2) )

class Beruka( FeCharacter ):
	def __init__( self ):
		self.level_ = 9
		self.baseClass_ = ClassDb.WyvernRider()
		self.baseStats_ = Utils.newFeStatLine( ('23', '13', '0', '14', '9', '10', '14', '7') )
		self.growthRates_ = Utils.newFeGrowthRate( ('45', '30', '10', '55', '30', '45', '40', '25') )
		self.maxStats_ = Utils.newFeStatLine( (0, -1, 0, 2, -2, 0, 2, -1) )

class Mitama( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.ShrineMaiden()
		self.baseStats_ = Utils.newFeStatLine( ('6', '7', '6', '6', '8', '10', '3', '5') )
		self.growthRates_ = Utils.newFeGrowthRate( ('45', '40', '35', '45', '50', '50', '30', '20') )
		self.maxStats_ = None

class Asugi( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.Ninja()
		self.baseStats_ = Utils.newFeStatLine( ('6', '7', '4', '7', '6', '9', '4', '9') )
		self.growthRates_ = Utils.newFeGrowthRate( ('40', '45', '50', '55', '45', '50', '30', '20') )
		self.maxStats_ = None

class Subaki( FeCharacter ):
	def __init__( self ):
		self.level_ = 5
		self.baseClass_ = ClassDb.SkyKnight()
		self.baseStats_ = Utils.newFeStatLine( ('22', '8', '0', '13', '10', '7', '9', '10') )
		self.growthRates_ = Utils.newFeGrowthRate( ('55', '30', '20', '50', '20', '25', '45', '5') )
		self.maxStats_ = Utils.newFeStatLine( (0, -1, 0, 2, -2, -1, 3, -1) )

class Hayato( FeCharacter ):
	def __init__( self ):
		self.level_ = 9
		self.baseClass_ = ClassDb.Diviner()
		self.baseStats_ = Utils.newFeStatLine( ('22', '2', '9', '9', '13', '11', '5', '7') )
		self.growthRates_ = Utils.newFeGrowthRate( ('50', '30', '40', '30', '45', '60', '40', '20') )
		self.maxStats_ = Utils.newFeStatLine( (0, 0, 1, -1, 2, 1, -1, -1) )

class Selena( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.Mercenary()
		self.baseStats_ = Utils.newFeStatLine( ('24', '12', '3', '12', '15', '9', '11', '8') )
		self.growthRates_ = Utils.newFeGrowthRate( ('40', '30', '5', '25', '45', '30', '45', '30') )
		self.maxStats_ = Utils.newFeStatLine( (0, -1, 0, -1, 2, 0, 1, 0) )

class Keaton( FeCharacter ):
	def __init__( self ):
		self.level_ = 15
		self.baseClass_ = ClassDb.Wolfskin()
		self.baseStats_ = Utils.newFeStatLine( ('35', '19', '0', '10', '13', '9', '16', '7') )
		self.growthRates_ = Utils.newFeGrowthRate( ('60', '60', '0', '20', '35', '30', '50', '25') )
		self.maxStats_ = Utils.newFeStatLine( (0, 3, 0, -2, -1, 0, 2, -1) )

class Niles( FeCharacter ):
	def __init__( self ):
		self.level_ = 14
		self.baseClass_ = ClassDb.Outlaw()
		self.baseStats_ = Utils.newFeStatLine( ('24', '11', '6', '11', '17', '7', '10', '16') )
		self.growthRates_ = Utils.newFeGrowthRate( ('40', '35', '20', '40', '50', '30', '30', '40') )
		self.maxStats_ = Utils.newFeStatLine( (0, -2, 0, -1, 3, 0, 0, 1) )

class Dwyer( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.Troubadour()
		self.baseStats_ = Utils.newFeStatLine( ('8', '7', '7', '2', '6', '4', '6', '7') )
		self.growthRates_ = Utils.newFeGrowthRate( ('45', '45', '30', '20', '30', '30', '30', '35') )
		self.maxStats_ = None

class Flora( FeCharacter ):
	def __init__( self ):
		self.level_ = 5
		self.baseClass_ = ClassDb.Maid()
		self.baseStats_ = Utils.newFeStatLine( ('29', '18', '16', '25', '15', '11', '14', '23') )
		self.growthRates_ = Utils.newFeGrowthRate( ('35', '40', '20', '45', '30', '35', '30', '30') )
		self.maxStats_ = Utils.newFeStatLine( (0, 1, -1, 2, 0, -1, 1, -1) )

class Hinata( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.Samurai()
		self.baseStats_ = Utils.newFeStatLine( ('26', '11', '0', '9', '14', '10', '12', '4') )
		self.growthRates_ = Utils.newFeGrowthRate( ('55', '35', '0', '25', '15', '45', '45', '15') )
		self.maxStats_ = Utils.newFeStatLine( (0, 1, 0, -1, -2, 0, 2, 0) )

class Velouria( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.Wolfskin()
		self.baseStats_ = Utils.newFeStatLine( ('7', '6', '0', '6', '6', '11', '9', '8') )
		self.growthRates_ = Utils.newFeGrowthRate( ('50', '50', '0', '40', '40', '35', '45', '30') )
		self.maxStats_ = None

class Charlotte( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.Fighter()
		self.baseStats_ = Utils.newFeStatLine( ('28', '15', '0', '10', '13', '9', '8', '2') )
		self.growthRates_ = Utils.newFeGrowthRate( ('65', '55', '0', '35', '50', '45', '20', '5') )
		self.maxStats_ = Utils.newFeStatLine( (0, 3, 0, 0, 2, 0, -2, -2) )

class Orochi( FeCharacter ):
	def __init__( self ):
		self.level_ = 7
		self.baseClass_ = ClassDb.Diviner()
		self.baseStats_ = Utils.newFeStatLine( ('20', '1', '10', '12', '7', '6', '5', '10') )
		self.growthRates_ = Utils.newFeGrowthRate( ('35', '5', '65', '50', '15', '35', '25', '45') )
		self.maxStats_ = Utils.newFeStatLine( (0, 0, 3, 2, -2, -1, -2, 1) )

class Benny( FeCharacter ):
	def __init__( self ):
		self.level_ = 15
		self.baseClass_ = ClassDb.Knight()
		self.baseStats_ = Utils.newFeStatLine( ('31', '15', '0', '15', '6', '12', '19', '10') )
		self.growthRates_ = Utils.newFeGrowthRate( ('50', '40', '0', '50', '10', '35', '55', '45') )
		self.maxStats_ = Utils.newFeStatLine( (0, 0, 0, 0, -3, 0, 3, 1) )

class Hinoka( FeCharacter ):
	def __init__( self ):
		self.level_ = 17
		self.baseClass_ = ClassDb.SkyKnight()
		self.baseStats_ = Utils.newFeStatLine( ('24', '14', '5', '18', '23', '16', '12', '23') )
		self.growthRates_ = Utils.newFeGrowthRate( ('45', '45', '15', '40', '45', '40', '35', '40') )
		self.maxStats_ = Utils.newFeStatLine( (0, 1, -1, -1, 1, 0, -1, 2) )

class Kaze( FeCharacter ):
	def __init__( self ):
		self.level_ = 3
		self.baseClass_ = ClassDb.Ninja()
		self.baseStats_ = Utils.newFeStatLine( ('19', '7', '0', '9', '12', '4', '5', '10') )
		self.growthRates_ = Utils.newFeGrowthRate( ('55', '40', '0', '45', '65', '20', '20', '35') )
		self.maxStats_ = Utils.newFeStatLine( (0, -2, 0, 2, 3, -2, -1, 1) )

class Nina( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.Outlaw()
		self.baseStats_ = Utils.newFeStatLine( ('5', '8', '5', '5', '5', '11', '3', '10') )
		self.growthRates_ = Utils.newFeGrowthRate( ('30', '45', '30', '35', '40', '50', '25', '45') )
		self.maxStats_ = None

class Arthur( FeCharacter ):
	def __init__( self ):
		self.level_ = 9
		self.baseClass_ = ClassDb.Fighter()
		self.baseStats_ = Utils.newFeStatLine( ('26', '13', '0', '10', '9', '1', '9', '4') )
		self.growthRates_ = Utils.newFeGrowthRate( ('50', '45', '0', '55', '35', '5', '45', '20') )
		self.maxStats_ = Utils.newFeStatLine( (0, 1, 0, 3, 0, -3, 1, -1) )

class Avatar( FeCharacter ):
	def __init__( self ):
		self.level_ = 1
		self.baseClass_ = ClassDb.NohrPrince()
		self.baseStats_ = Utils.newFeStatLine( ('19', '7', '4', '7', '6', '5', '6', '2') )
		self.growthRates_ = Utils.newFeGrowthRate( ('45', '45', '30', '40', '45', '45', '35', '25') )
		self.maxStats_ = None

class Midori( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.Apothecary()
		self.baseStats_ = Utils.newFeStatLine( ('8', '6', '2', '10', '4', '10', '4', '2') )
		self.growthRates_ = Utils.newFeGrowthRate( ('45', '35', '5', '55', '35', '50', '30', '20') )
		self.maxStats_ = None

class Sophie( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.Cavalier()
		self.baseStats_ = Utils.newFeStatLine( ('8', '6', '2', '7', '6', '7', '4', '6') )
		self.growthRates_ = Utils.newFeGrowthRate( ('35', '35', '10', '55', '50', '35', '25', '35') )
		self.maxStats_ = None

class Azura( FeCharacter ):
	def __init__( self ):
		self.level_ = 1
		self.baseClass_ = ClassDb.Songstress()
		self.baseStats_ = Utils.newFeStatLine( ('16', '5', '2', '8', '8', '6', '4', '7') )
		self.growthRates_ = Utils.newFeGrowthRate( ('25', '50', '25', '60', '60', '40', '15', '35') )
		self.maxStats_ = Utils.newFeStatLine( (0, 0, 0, 1, 3, 0, -3, 0) )

class Ryoma( FeCharacter ):
	def __init__( self ):
		self.level_ = 4
		self.baseClass_ = ClassDb.Swordmaster()
		self.baseStats_ = Utils.newFeStatLine( ('36', '20', '2', '18', '24', '20', '16', '13') )
		self.growthRates_ = Utils.newFeGrowthRate( ('50', '45', '0', '50', '45', '40', '35', '25') )
		self.maxStats_ = Utils.newFeStatLine( (0, 1, 0, 2, 1, 1, -2, -2) )

class Saizo( FeCharacter ):
	def __init__( self ):
		self.level_ = 9
		self.baseClass_ = ClassDb.Ninja()
		self.baseStats_ = Utils.newFeStatLine( ('23', '11', '3', '15', '12', '9', '9', '8') )
		self.growthRates_ = Utils.newFeGrowthRate( ('40', '50', '45', '60', '30', '55', '45', '10') )
		self.maxStats_ = Utils.newFeStatLine( (0, 1, 0, 3, -2, 0, 1, -2) )

class Ignatius( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.Knight()
		self.baseStats_ = Utils.newFeStatLine( ('8', '7', '0', '6', '4', '7', '6', '7') )
		self.growthRates_ = Utils.newFeGrowthRate( ('40', '50', '0', '40', '30', '55', '45', '35') )
		self.maxStats_ = None

class Takumi( FeCharacter ):
	def __init__( self ):
		self.level_ = 11
		self.baseClass_ = ClassDb.Archer()
		self.baseStats_ = Utils.newFeStatLine( ('26', '13', '0', '17', '11', '13', '10', '4') )
		self.growthRates_ = Utils.newFeGrowthRate( ('50', '35', '0', '60', '40', '45', '35', '20') )
		self.maxStats_ = Utils.newFeStatLine( (0, 1, 0, 3, -2, 1, 0, -2) )

class Odin( FeCharacter ):
	def __init__( self ):
		self.level_ = 12
		self.baseClass_ = ClassDb.DarkMage()
		self.baseStats_ = Utils.newFeStatLine( ('24', '8', '12', '12', '10', '12', '7', '10') )
		self.growthRates_ = Utils.newFeGrowthRate( ('55', '35', '30', '55', '35', '60', '40', '20') )
		self.maxStats_ = Utils.newFeStatLine( (0, 0, 1, 1, -1, 1, 0, -1) )

class Shigure( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.SkyKnight()
		self.baseStats_ = Utils.newFeStatLine( ('9', '6', '1', '7', '7', '5', '8', '7') )
		self.growthRates_ = Utils.newFeGrowthRate( ('35', '45', '5', '45', '35', '25', '35', '25') )
		self.maxStats_ = Utils.newFeStatLine( (0, 3, -1, 4, 4, 0, -2, 0) )

class Kagero( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.Ninja()
		self.baseStats_ = Utils.newFeStatLine( ('22', '15', '0', '10', '12', '7', '9', '10') )
		self.growthRates_ = Utils.newFeGrowthRate( ('30', '65', '0', '20', '50', '30', '25', '40') )
		self.maxStats_ = Utils.newFeStatLine( (0, 3, 0, -1, -1, 0, -1, 1) )

class Percy( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.WyvernRider()
		self.baseStats_ = Utils.newFeStatLine( ('6', '4', '0', '6', '6', '15', '8', '4') )
		self.growthRates_ = Utils.newFeGrowthRate( ('30', '30', '5', '45', '40', '75', '55', '15') )
		self.maxStats_ = None

class Scarlet( FeCharacter ):
	def __init__( self ):
		self.level_ = 3
		self.baseClass_ = ClassDb.WyvernLord()
		self.baseStats_ = Utils.newFeStatLine( ('32', '22', '4', '18', '19', '14', '23', '7') )
		self.growthRates_ = Utils.newFeGrowthRate( ('30', '45', '20', '40', '50', '40', '25', '20') )
		self.maxStats_ = Utils.newFeStatLine( (0, 2, 0, 0, 1, -1, 1, -2) )

class Oboro( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.SpearFighter()
		self.baseStats_ = Utils.newFeStatLine( ('25', '13', '0', '11', '12', '11', '13', '8') )
		self.growthRates_ = Utils.newFeGrowthRate( ('30', '40', '20', '40', '40', '40', '40', '30') )
		self.maxStats_ = Utils.newFeStatLine( (0, 1, -1, 1, 1, -1, 1, -1) )

class Hisame( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.Samurai()
		self.baseStats_ = Utils.newFeStatLine( ('6', '6', '1', '7', '5', '4', '5', '4') )
		self.growthRates_ = Utils.newFeGrowthRate( ('50', '40', '0', '40', '40', '25', '30', '20') )
		self.maxStats_ = None

class Setsuna( FeCharacter ):
	def __init__( self ):
		self.level_ = 11
		self.baseClass_ = ClassDb.Archer()
		self.baseStats_ = Utils.newFeStatLine( ('25', '12', '0', '15', '17', '11', '8', '10') )
		self.growthRates_ = Utils.newFeGrowthRate( ('30', '20', '0', '30', '60', '30', '15', '40') )
		self.maxStats_ = Utils.newFeStatLine( (0, 0, 0, 1, 3, -1, -1, -1) )

class Elise( FeCharacter ):
	def __init__( self ):
		self.level_ = 7
		self.baseClass_ = ClassDb.Troubadour()
		self.baseStats_ = Utils.newFeStatLine( ('20', '2', '13', '7', '11', '16', '4', '13') )
		self.growthRates_ = Utils.newFeGrowthRate( ('30', '5', '65', '25', '55', '70', '15', '40') )
		self.maxStats_ = Utils.newFeStatLine( (0, -1, 3, -2, 1, 1, -3, 1) )

class Laslow( FeCharacter ):
	def __init__( self ):
		self.level_ = 16
		self.baseClass_ = ClassDb.Mercenary()
		self.baseStats_ = Utils.newFeStatLine( ('30', '17', '0', '19', '16', '16', '12', '8') )
		self.growthRates_ = Utils.newFeGrowthRate( ('50', '45', '0', '45', '30', '55', '35', '25') )
		self.maxStats_ = Utils.newFeStatLine( (0, 1, 0, 2, -1, 1, -1, -1) )

class Forrest( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.Troubadour()
		self.baseStats_ = Utils.newFeStatLine( ('8', '5', '9', '1', '4', '5', '6', '13') )
		self.growthRates_ = Utils.newFeGrowthRate( ('55', '15', '65', '20', '35', '25', '25', '55') )
		self.maxStats_ = None

class Reina( FeCharacter ):
	def __init__( self ):
		self.level_ = 1
		self.baseClass_ = ClassDb.KinshiKnight()
		self.baseStats_ = Utils.newFeStatLine( ('28', '18', '4', '16', '21', '15', '10', '13') )
		self.growthRates_ = Utils.newFeGrowthRate( ('40', '45', '5', '20', '45', '10', '20', '10') )
		self.maxStats_ = Utils.newFeStatLine( (0, 2, 0, 0, 2, -1, -2, -1) )

class Siegbert( FeCharacter ):
	def __init__( self ):
		self.level_ = 10
		self.baseClass_ = ClassDb.Cavalier()
		self.baseStats_ = Utils.newFeStatLine( ('7', '5', '2', '7', '6', '7', '6', '3') )
		self.growthRates_ = Utils.newFeGrowthRate( ('40', '45', '5', '45', '45', '45', '35', '20') )
		self.maxStats_ = None

