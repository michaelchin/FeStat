import math
import CharDb
import ClassDb
import FeStatUtils as Utils

# C const-ish declaration in python...
OUTPUT_LINE_LEN = 40


class Character( object ):
   def __init__( self, name, classChangeDict ):
      self.name_ = name
      self.classChangeDict_ = classChangeDict
      self.db_ = getattr( CharDb, name )()
      self.class_ = self.db_.baseClass()
      self.level_ = self.db_.baseLevel()
      if self.class_.promoted():
         self.level_ += 20
      self.stat_ = self.db_.baseStats()
      self.maxStats_ = self.db_.maxStats()
      self.growthRates_ = self.db_.growthRates()

   def fmtStat( self, stat, maxStat ):
      if stat < maxStat:
         return "%2d" % stat
      else:
         return "\033[92;1m%2d\033[0m" % maxStat
   # start printStats {
   def printStats( self, start=None, end=41, printMore=False ):
      if not start or start < self.level_:
         start = self.level_
      print self.name_
      if( printMore ):
         # TODO: printMore = print growth rates, max stats, base stats, etc...
         if hasattr( self, "bsParam_" ):
            print self.bsParam_
         print self.growthRates_
         pass
      # Print header
      print OUTPUT_LINE_LEN * '-'
      print "Lv |  Hp Str Mag Skl Spd Lck Def Res"
      currClass = self.class_
      currStats = self.stat_
      for level in range( start, end ):
         if level in self.classChangeDict_:
            oldClass = currClass
            currClass = getattr( ClassDb, self.classChangeDict_[ level ] )()
            statAdjust = tuple( int( curr - old ) for ( curr, old ) in
                                zip( currClass.baseStats(),
                                     oldClass.baseStats() ) )
         else:
            statAdjust = tuple( len( Utils.stats ) * [ 0 ] )
         levelDelta = level - self.level_
         if level == 21 and level in self.classChangeDict_:
            # No level-up on promotion
            totalGr = Utils.newFeStatLine( tuple( len( Utils.stats ) * [ 0 ] ) )
         else:
            totalGr = Utils.newFeStatLine( tuple( sum( x ) / 100.0
                                           for x in zip( currClass.growthRates(),
                                                         self.growthRates_ ) ) )
         currStats = Utils.newFeStatLine(
                        tuple( currStats[ i ] + totalGr[ i ] + statAdjust[ i ]
                               for i in range( len( Utils.stats ) ) ) )
         fmtStats = [ self.fmtStat( math.floor( currStats[ i ] ),
                                    math.floor( currClass.maxStats()[ i ] +
                                                self.maxStats_[ i ] ) )
                      for i in range( len( Utils.stats ) ) ]
         # TODO: investigate potential inaccuracies/additional features:
         #  1. First level-up: currently transition from level base->base+1 is
         #     2*totalGrowths--essentially an extra level-up. This isn't the case
         #     in-game, but the in-game case on first level-ups is not transparent
         #     either (e.g. Corrin got +1 Res from lv1->2??)
         #  2. Level-up after class-change/promotion: is the running-point total
         #     maintained or reset to zero/arbitrary amount?
         #  3. Red/blue numbers based on stat decrease/increase?
         print "%2d |  %s  %s  %s  %s  %s  %s  %s  %s   %s" % \
            tuple( [ level ] + fmtStats + [ currClass.__class__.__name__ ] )
      print OUTPUT_LINE_LEN * '-'
      # Look at all this lovely code-duplication
      print "%s |  %s  %s  %s  %s  %s  %s  %s  %s   %s" % \
         tuple( [ "Mx" ] +
                [ self.fmtStat( math.floor( currClass.maxStats()[ i ] +
                                            self.maxStats_[ i ] ),
                                math.floor( currClass.maxStats()[ i ] +
                                            self.maxStats_[ i ] ) )
                  for i in range( len( Utils.stats ) ) ] +
                [ currClass.__class__.__name__ ] )
      print OUTPUT_LINE_LEN * '='
      print
   # end printStats }
      
class Avatar( Character ):
   def __init__( self, name, classChangeDict, boon, bane ):
      # Enforce that boon/bane be given in FeStatUtils.stats format (Obviously there
      # are better designs but none that I want to come up with now. Perks of being
      #  your own customer)
      if boon not in Utils.stats or bane not in Utils.stats:
         assert False, "Provide boon/bane as one of FeStatUtils.stats"
      if boon == bane:
         assert False, "boon and bane must be different stats"
      super( Avatar, self ).__init__( "Avatar", classChangeDict )
      # TODO: base stats, max stats, and growth rates need to be adjusted for boon+bane
      self.name_ = name
      self.stat_ = Utils.newFeStatLine( tuple( sum( x ) for x in
                                               zip( self.db_.baseStats(),
                                                    Utils.avatarBoon[ boon ][ 0 ],
                                                    Utils.avatarBane[ bane ][ 0 ] ) ) )
      self.maxStats_ = Utils.newFeStatLine(
                          tuple( sum( x ) for x in
                                 zip( Utils.avatarBoon[ boon ][ 1 ],
                                      Utils.avatarBane[ bane ][ 1 ] ) ) )
      self.growthRates_ = Utils.newFeGrowthRate(
                             tuple( sum( x ) for x in
                                    zip( self.db_.growthRates(),
                                         Utils.avatarBoon[ boon ][ 2 ],
                                         Utils.avatarBane[ bane ][ 2 ] ) ) )

class ChildCharacter( Character ):
   def __init__( self, name, classChangeDict, fixedParent, varParent ):
      # So, CharDb *should* have fixedParent as an attribute for the child characters,
      # but I didn't design for that initially, and I don't want to add it into
      # FeExtractStats now. This is the work-around for now, so don't pass in the
      # wrong parent.
      #
      # So here's the catch. Because the Avatar has variable growths and max stats,
      # the parent parameters need to be fully-initialized Character objects, so that
      # the child attributes are computed as closely as possible
      #
      # Children characters are a pain. The only reason I'm bothering is because it'd
      # be cool to use Corrin as a strength-based unit and Kana as primarily a
      # Dragonstone-user. Hopefully after going through all this, I can find a
      # parent-combination that makes it viable.
      super( ChildCharacter, self ).__init__( name, classChangeDict )

      # GrowthRate is the easiest one of the children's stats to compute.
      self.growthRates_ = Utils.newFeGrowthRate(
                             tuple( sum( x ) / 2 for x in
                                    zip( varParent.growthRates_,
                                         self.db_.growthRates() ) ) )
      # MaxStats aren't bad. It'd be much nicer if fixedParent wasn't a ctor param.
      self.maxStats_ = Utils.newFeStatLine(
                          tuple( sum( x ) for x in
                                 zip( fixedParent.maxStats_,
                                      varParent.maxStats_,
                                      tuple( [ 0 ] + len( Utils.stats ) * [ 1 ] ) ) ) )
      # BaseStats are *AWFUL*. Their based on the level you recruit them AND the stats
      # of BOTH parents AT THE TIME YOU RECRUIT THEM, so to estimate the average
      # properly, we need information that we can't trivially get. Luckily (I guess not
      # a good choice of words), there's an upper limit to the stat-contribution from
      # the parents, so estimate using that, and the base stats based on level-
      # recruitment are generated using their growth rates, so we should be pretty
      # close estimating from Lv10.
      self.bsParam_ = Utils.newFeStatLine(
                           tuple( math.floor( x / 10 ) + 2 for x in
                                  self.db_.baseStats() ) )
      self.stat_= Utils.newFeStatLine(
                     tuple( sum( x ) for x in
                            zip( self.db_.baseStats(),
                                 self.class_.baseStats(), 
                                 self.bsParam_ ) ) )
