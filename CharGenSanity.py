import CharGen

''' {
xander = CharGen.Character( "Xander", {} )
xander.printStats()

hana = CharGen.Character( "Hana", { 21: 'Swordmaster' } )
hana.printStats()

azura = CharGen.Character( "Azura", {} )
azura.printStats()
} '''

hinoka = CharGen.Character( "Hinoka", { 18: 'SpearFighter', 21: 'SpearMaster' } )
hinoka.printStats()

azama = CharGen.Character( "Azama", { 14: 'OniSavage',
                                      21: 'OniChieftain' } )
# azama.printStats()

corrin = CharGen.Avatar( "Corrin", { 11: 'Mercenary',
                                     19: 'Samurai',
                                     21: 'Hero',
                                     27: 'Swordmaster',
                                     28: 'Hero',
                                     35: 'HoshidoNoble' }, 'spd', 'Def' )
# corrin.printStats()

sakura = CharGen.Character( "Sakura", { 21: 'Priestess' } )
# sakura.printStats()

saizo = CharGen.Character( "Saizo", { 21: 'MasterNinja' } )
# saizo.printStats()
'''{
elise = CharGen.Character( "Elise", { 21: 'Strategist' } )
elise.printStats()

orochi = CharGen.Character( "Orochi", { 21: 'Onmyoji' } )
orochi.printStats()

nyx = CharGen.Character( "Nyx", { 21: 'Sorcerer' } )
nyx.printStats()

leo = CharGen.Character( "Leo", { 23: 'Sorcerer' } )
leo.printStats()

hayato = CharGen.Character( "Hayato", { 21: 'Onmyoji' } )
hayato.printStats()

kan0 = CharGen.ChildCharacter( "Kana", { 11: 'DarkMage',
                                         21: 'Sorcerer',
                                         40: 'NohrNoble' },
                               corrin, saizo )
#                              corrin, sakura )
kan0.printStats( printMore=True )

kan1 = CharGen.ChildCharacter( "Kana", { 11: 'DarkMage',
                                         21: 'Sorcerer',
                                         40: 'NohrNoble' },
                               corrin, elise )
kan1.printStats( printMore=True )

kan2 = CharGen.ChildCharacter( "Kana", { 11: 'DarkMage',
                                         21: 'Sorcerer',
                                         40: 'NohrNoble' },
                               corrin, orochi )
kan2.printStats( printMore=True )

kan3 = CharGen.ChildCharacter( "Kana", { 11: 'DarkMage',
                                         21: 'Sorcerer',
                                         40: 'NohrNoble' },
                               corrin, nyx )
kan3.printStats( printMore=True )

kan4 = CharGen.ChildCharacter( "Kana", { 11: 'DarkMage',
                                         21: 'Sorcerer',
                                         40: 'NohrNoble' },
                               corrin, leo )
kan4.printStats( printMore=True )
} '''
kan5 = CharGen.ChildCharacter( "Kana", { 21: 'NohrNoble',
                                         26: 'Sorcerer',
                                         40: 'NohrNoble' },
                               corrin, sakura )
# kan5.printStats( printMore=True )

camilla = CharGen.Character( "Camilla", { 22: 'WyvernLord' } )
camilla.printStats()
