import FeStatUtils as Utils

class FeClass( object ):
	def __init__( self ):
		self.baseStats_ = None
		self.growthRates_ = None
		self.maxStats_ = None
		self.promoted_ = False
	def baseStats( self ):
		return self.baseStats_
	def growthRates( self ):
		return self.growthRates_
	def maxStats( self ):
		return self.maxStats_
	def promoted( self ):
		return self.promoted_

class FalconKnight( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('18', '5', '4', '6', '10', '5', '5', '9') )
		self.growthRates_ = Utils.newFeGrowthRate( ('0', '10', '10', '10', '15', '20', '0', '20') )
		self.maxStats_ = Utils.newFeStatLine( ('55', '28', '27', '30', '34', '35', '27', '35') )
		self.promoted_ = True

class MaligKnight( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('18', '7', '6', '6', '5', '0', '8', '6') )
		self.growthRates_ = Utils.newFeGrowthRate( ('0', '15', '15', '10', '5', '0', '10', '15') )
		self.maxStats_ = Utils.newFeStatLine( ('55', '31', '30', '28', '27', '25', '31', '31') )
		self.promoted_ = True

class Troubadour( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('15', '0', '3', '7', '5', '4', '1', '4') )
		self.growthRates_ = Utils.newFeGrowthRate( ('0', '0', '10', '20', '10', '15', '0', '15') )
		self.maxStats_ = Utils.newFeStatLine( ('35', '16', '19', '24', '20', '23', '16', '21') )
		self.promoted_ = False

class SpearFighter( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('17', '6', '0', '6', '6', '2', '5', '2') )
		self.growthRates_ = Utils.newFeGrowthRate( ('15', '15', '0', '15', '15', '5', '10', '5') )
		self.maxStats_ = Utils.newFeStatLine( ('40', '22', '15', '23', '22', '21', '22', '21') )
		self.promoted_ = False

class Wolfskin( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('19', '8', '0', '4', '6', '0', '4', '0') )
		self.growthRates_ = Utils.newFeGrowthRate( ('20', '20', '0', '5', '15', '5', '10', '0') )
		self.maxStats_ = Utils.newFeStatLine( ('45', '24', '15', '18', '22', '17', '21', '15') )
		self.promoted_ = False

class ShrineMaiden( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('16', '0', '3', '5', '5', '4', '2', '5') )
		self.growthRates_ = Utils.newFeGrowthRate( ('0', '5', '10', '10', '15', '15', '0', '20') )
		self.maxStats_ = Utils.newFeStatLine( ('35', '18', '21', '20', '22', '23', '17', '24') )
		self.promoted_ = False

class SpearMaster( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('18', '9', '0', '8', '8', '3', '7', '3') )
		self.growthRates_ = Utils.newFeGrowthRate( ('15', '15', '0', '15', '15', '5', '10', '5') )
		self.maxStats_ = Utils.newFeStatLine( ('60', '34', '25', '33', '32', '29', '30', '29') )
		self.promoted_ = True

class Maid( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('18', '4', '5', '9', '8', '4', '5', '4') )
		self.growthRates_ = Utils.newFeGrowthRate( ('0', '10', '10', '15', '15', '10', '5', '10') )
		self.maxStats_ = Utils.newFeStatLine( ('50', '28', '31', '33', '33', '32', '29', '29') )
		self.promoted_ = True

class NohrPrince( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('17', '7', '3', '4', '5', '2', '5', '2') )
		self.growthRates_ = Utils.newFeGrowthRate( ('15', '15', '10', '10', '10', '10', '10', '5') )
		self.maxStats_ = Utils.newFeStatLine( ('40', '23', '17', '19', '21', '22', '21', '19') )
		self.promoted_ = False

class Mechanist( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('18', '7', '0', '9', '7', '2', '6', '6') )
		self.growthRates_ = Utils.newFeGrowthRate( ('10', '10', '0', '15', '10', '5', '5', '15') )
		self.maxStats_ = Utils.newFeStatLine( ('60', '30', '25', '33', '30', '30', '31', '31') )
		self.promoted_ = True

class Butler( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('18', '4', '5', '9', '8', '4', '5', '4') )
		self.growthRates_ = Utils.newFeGrowthRate( ('0', '10', '10', '15', '15', '10', '5', '10') )
		self.maxStats_ = Utils.newFeStatLine( ('50', '28', '31', '33', '33', '32', '29', '29') )
		self.promoted_ = True

class Songstress( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('16', '3', '0', '6', '5', '3', '2', '3') )
		self.growthRates_ = Utils.newFeGrowthRate( ('0', '10', '0', '20', '20', '20', '0', '0') )
		self.maxStats_ = Utils.newFeStatLine( ('45', '28', '27', '31', '31', '35', '27', '28') )
		self.promoted_ = False

class Strategist( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('16', '0', '7', '6', '7', '5', '2', '7') )
		self.growthRates_ = Utils.newFeGrowthRate( ('0', '0', '15', '5', '10', '20', '0', '15') )
		self.maxStats_ = Utils.newFeStatLine( ('45', '25', '33', '28', '31', '33', '25', '32') )
		self.promoted_ = True

class DarkMage( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('16', '0', '6', '3', '3', '1', '3', '5') )
		self.growthRates_ = Utils.newFeGrowthRate( ('0', '10', '20', '0', '10', '0', '5', '10') )
		self.maxStats_ = Utils.newFeStatLine( ('35', '19', '24', '16', '19', '18', '19', '22') )
		self.promoted_ = False

class Sniper( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('19', '7', '0', '10', '9', '3', '6', '2') )
		self.growthRates_ = Utils.newFeGrowthRate( ('10', '15', '0', '20', '15', '5', '10', '0') )
		self.maxStats_ = Utils.newFeStatLine( ('55', '31', '25', '35', '33', '30', '31', '28') )
		self.promoted_ = True

class Onmyoji( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('16', '0', '7', '6', '7', '2', '3', '6') )
		self.growthRates_ = Utils.newFeGrowthRate( ('0', '0', '20', '10', '15', '0', '0', '15') )
		self.maxStats_ = Utils.newFeStatLine( ('45', '25', '33', '31', '32', '27', '25', '31') )
		self.promoted_ = True

class Paladin( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('19', '8', '1', '7', '7', '4', '7', '6') )
		self.growthRates_ = Utils.newFeGrowthRate( ('10', '15', '0', '10', '10', '15', '10', '10') )
		self.maxStats_ = Utils.newFeStatLine( ('60', '31', '26', '30', '30', '32', '32', '32') )
		self.promoted_ = True

class Hero( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('20', '8', '0', '10', '8', '3', '7', '2') )
		self.growthRates_ = Utils.newFeGrowthRate( ('20', '15', '0', '20', '15', '5', '10', '0') )
		self.maxStats_ = Utils.newFeStatLine( ('60', '32', '25', '35', '32', '31', '30', '27') )
		self.promoted_ = True

class Basara( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('20', '7', '5', '7', '7', '5', '7', '6') )
		self.growthRates_ = Utils.newFeGrowthRate( ('20', '10', '10', '10', '10', '15', '5', '10') )
		self.maxStats_ = Utils.newFeStatLine( ('65', '31', '30', '30', '31', '35', '30', '32') )
		self.promoted_ = True

class NineTails( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('19', '6', '2', '9', '10', '5', '2', '8') )
		self.growthRates_ = Utils.newFeGrowthRate( ('10', '10', '0', '15', '20', '10', '0', '20') )
		self.maxStats_ = Utils.newFeStatLine( ('55', '29', '29', '33', '34', '33', '27', '34') )
		self.promoted_ = True

class Villager( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('17', '5', '0', '4', '5', '3', '4', '0') )
		self.growthRates_ = Utils.newFeGrowthRate( ('10', '10', '0', '10', '10', '20', '10', '0') )
		self.maxStats_ = Utils.newFeStatLine( ('35', '19', '15', '19', '19', '22', '18', '15') )
		self.promoted_ = False

class MasterNinja( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('17', '5', '0', '10', '11', '2', '4', '8') )
		self.growthRates_ = Utils.newFeGrowthRate( ('5', '5', '0', '20', '20', '0', '5', '20') )
		self.maxStats_ = Utils.newFeStatLine( ('55', '27', '25', '35', '35', '28', '26', '34') )
		self.promoted_ = True

class OniSavage( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('18', '6', '1', '2', '5', '0', '7', '1') )
		self.growthRates_ = Utils.newFeGrowthRate( ('20', '20', '10', '0', '10', '0', '20', '0') )
		self.maxStats_ = Utils.newFeStatLine( ('45', '24', '19', '16', '20', '17', '23', '18') )
		self.promoted_ = False

class HoshidoNoble( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('19', '10', '4', '5', '6', '4', '7', '3') )
		self.growthRates_ = Utils.newFeGrowthRate( ('15', '15', '10', '10', '10', '10', '15', '0') )
		self.maxStats_ = Utils.newFeStatLine( ('60', '34', '28', '29', '30', '33', '31', '28') )
		self.promoted_ = True

class MasterOfArms( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('20', '8', '0', '6', '9', '3', '7', '3') )
		self.growthRates_ = Utils.newFeGrowthRate( ('20', '15', '0', '10', '10', '10', '10', '0') )
		self.maxStats_ = Utils.newFeStatLine( ('65', '33', '25', '30', '30', '31', '31', '28') )
		self.promoted_ = True

class Ninja( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('16', '3', '0', '8', '8', '1', '3', '3') )
		self.growthRates_ = Utils.newFeGrowthRate( ('5', '5', '0', '20', '20', '0', '5', '15') )
		self.maxStats_ = Utils.newFeStatLine( ('35', '17', '15', '25', '25', '18', '19', '20') )
		self.promoted_ = False

class Berserker( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('24', '12', '0', '8', '9', '0', '5', '0') )
		self.growthRates_ = Utils.newFeGrowthRate( ('30', '25', '0', '15', '15', '0', '0', '0') )
		self.maxStats_ = Utils.newFeStatLine( ('70', '40', '25', '32', '33', '25', '27', '25') )
		self.promoted_ = True

class Sorcerer( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('17', '0', '9', '4', '6', '1', '5', '8') )
		self.growthRates_ = Utils.newFeGrowthRate( ('0', '0', '25', '0', '10', '0', '5', '15') )
		self.maxStats_ = Utils.newFeStatLine( ('50', '25', '35', '26', '29', '26', '29', '33') )
		self.promoted_ = True

class WyvernLord( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('19', '8', '0', '9', '6', '3', '10', '1') )
		self.growthRates_ = Utils.newFeGrowthRate( ('10', '15', '0', '15', '10', '5', '20', '0') )
		self.maxStats_ = Utils.newFeStatLine( ('60', '33', '25', '33', '29', '28', '35', '26') )
		self.promoted_ = True

class Outlaw( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('16', '3', '1', '4', '8', '1', '2', '4') )
		self.growthRates_ = Utils.newFeGrowthRate( ('0', '10', '5', '10', '20', '0', '0', '20') )
		self.maxStats_ = Utils.newFeStatLine( ('35', '19', '18', '20', '24', '18', '17', '22') )
		self.promoted_ = False

class Swordmaster( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('18', '6', '2', '7', '11', '4', '5', '5') )
		self.growthRates_ = Utils.newFeGrowthRate( ('10', '10', '5', '15', '20', '15', '0', '10') )
		self.maxStats_ = Utils.newFeStatLine( ('55', '30', '28', '32', '35', '33', '27', '31') )
		self.promoted_ = True

class GreatKnight( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('21', '10', '0', '6', '6', '3', '10', '2') )
		self.growthRates_ = Utils.newFeGrowthRate( ('20', '20', '0', '10', '5', '5', '20', '0') )
		self.maxStats_ = Utils.newFeStatLine( ('65', '35', '25', '29', '27', '28', '37', '28') )
		self.promoted_ = True

class NohrNoble( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('18', '8', '6', '4', '7', '2', '6', '6') )
		self.growthRates_ = Utils.newFeGrowthRate( ('15', '10', '15', '5', '15', '5', '5', '15') )
		self.maxStats_ = Utils.newFeStatLine( ('60', '32', '31', '28', '32', '27', '29', '32') )
		self.promoted_ = True

class Merchant( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('20', '8', '0', '6', '5', '4', '8', '5') )
		self.growthRates_ = Utils.newFeGrowthRate( ('20', '20', '0', '10', '5', '15', '10', '5') )
		self.maxStats_ = Utils.newFeStatLine( ('65', '33', '25', '29', '28', '32', '33', '30') )
		self.promoted_ = True

class Diviner( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('15', '0', '4', '5', '6', '1', '1', '3') )
		self.growthRates_ = Utils.newFeGrowthRate( ('0', '5', '15', '10', '15', '5', '0', '10') )
		self.maxStats_ = Utils.newFeStatLine( ('35', '17', '22', '20', '23', '19', '16', '20') )
		self.promoted_ = False

class Fighter( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('19', '7', '0', '6', '6', '2', '4', '1') )
		self.growthRates_ = Utils.newFeGrowthRate( ('20', '20', '0', '15', '15', '5', '5', '0') )
		self.maxStats_ = Utils.newFeStatLine( ('45', '25', '15', '23', '22', '21', '19', '18') )
		self.promoted_ = False

class Knight( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('19', '8', '0', '5', '3', '3', '8', '1') )
		self.growthRates_ = Utils.newFeGrowthRate( ('20', '20', '0', '15', '5', '10', '20', '0') )
		self.maxStats_ = Utils.newFeStatLine( ('45', '24', '15', '22', '17', '22', '26', '18') )
		self.promoted_ = False

class Wolfssegner( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('22', '11', '0', '6', '7', '1', '7', '1') )
		self.growthRates_ = Utils.newFeGrowthRate( ('20', '20', '0', '5', '15', '5', '10', '0') )
		self.maxStats_ = Utils.newFeStatLine( ('65', '36', '25', '29', '31', '26', '32', '26') )
		self.promoted_ = True

class KinshiKnight( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('17', '4', '1', '9', '8', '5', '4', '7') )
		self.growthRates_ = Utils.newFeGrowthRate( ('0', '5', '0', '15', '15', '15', '0', '15') )
		self.maxStats_ = Utils.newFeStatLine( ('50', '27', '26', '33', '31', '34', '25', '31') )
		self.promoted_ = True

class Adventurer( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('17', '4', '6', '6', '10', '2', '3', '8') )
		self.growthRates_ = Utils.newFeGrowthRate( ('0', '5', '15', '5', '20', '0', '0', '20') )
		self.maxStats_ = Utils.newFeStatLine( ('50', '27', '31', '27', '34', '27', '25', '34') )
		self.promoted_ = True

class Monk( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('16', '0', '3', '5', '5', '4', '2', '5') )
		self.growthRates_ = Utils.newFeGrowthRate( ('0', '5', '10', '10', '15', '15', '0', '20') )
		self.maxStats_ = Utils.newFeStatLine( ('35', '18', '21', '20', '22', '23', '17', '24') )
		self.promoted_ = False

class WyvernRider( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('17', '6', '0', '5', '4', '2', '7', '0') )
		self.growthRates_ = Utils.newFeGrowthRate( ('10', '15', '5', '10', '10', '5', '20', '0') )
		self.maxStats_ = Utils.newFeStatLine( ('40', '22', '17', '21', '20', '19', '24', '15') )
		self.promoted_ = False

class Priestess( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('19', '6', '7', '6', '9', '5', '5', '8') )
		self.growthRates_ = Utils.newFeGrowthRate( ('10', '10', '10', '5', '15', '15', '0', '20') )
		self.maxStats_ = Utils.newFeStatLine( ('50', '29', '32', '30', '33', '34', '26', '34') )
		self.promoted_ = False

class Samurai( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('17', '4', '0', '5', '8', '3', '3', '3') )
		self.growthRates_ = Utils.newFeGrowthRate( ('10', '10', '0', '15', '20', '15', '0', '10') )
		self.maxStats_ = Utils.newFeStatLine( ('40', '20', '16', '23', '25', '24', '18', '20') )
		self.promoted_ = False

class Archer( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('17', '5', '0', '7', '5', '2', '4', '1') )
		self.growthRates_ = Utils.newFeGrowthRate( ('10', '15', '0', '15', '15', '5', '10', '0') )
		self.maxStats_ = Utils.newFeStatLine( ('40', '21', '15', '23', '21', '20', '20', '17') )
		self.promoted_ = False

class Cavalier( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('17', '6', '0', '5', '5', '3', '5', '3') )
		self.growthRates_ = Utils.newFeGrowthRate( ('10', '15', '0', '10', '10', '15', '10', '5') )
		self.maxStats_ = Utils.newFeStatLine( ('40', '22', '15', '21', '20', '24', '22', '21') )
		self.promoted_ = False

class Blacksmith( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('21', '8', '0', '9', '8', '3', '8', '2') )
		self.growthRates_ = Utils.newFeGrowthRate( ('20', '15', '0', '15', '10', '5', '15', '0') )
		self.maxStats_ = Utils.newFeStatLine( ('65', '33', '25', '32', '31', '30', '32', '27') )
		self.promoted_ = True

class GreatMaster( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('19', '8', '6', '6', '8', '5', '6', '7') )
		self.growthRates_ = Utils.newFeGrowthRate( ('10', '15', '5', '5', '15', '15', '10', '10') )
		self.maxStats_ = Utils.newFeStatLine( ('55', '32', '30', '31', '33', '32', '28', '32') )
		self.promoted_ = True

class DarkKnight( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('19', '8', '6', '6', '5', '3', '8', '6') )
		self.growthRates_ = Utils.newFeGrowthRate( ('15', '20', '10', '5', '5', '5', '15', '5') )
		self.maxStats_ = Utils.newFeStatLine( ('55', '32', '31', '28', '27', '31', '34', '30') )
		self.promoted_ = True

class Kitsune( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('16', '5', '1', '6', '8', '4', '1', '4') )
		self.growthRates_ = Utils.newFeGrowthRate( ('10', '10', '0', '15', '20', '10', '0', '20') )
		self.maxStats_ = Utils.newFeStatLine( ('40', '20', '18', '23', '24', '24', '18', '23') )
		self.promoted_ = False

class Mercenary( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('17', '5', '0', '7', '6', '2', '5', '2') )
		self.growthRates_ = Utils.newFeGrowthRate( ('10', '15', '0', '20', '15', '5', '10', '5') )
		self.maxStats_ = Utils.newFeStatLine( ('40', '22', '15', '24', '22', '20', '21', '19') )
		self.promoted_ = False

class General( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('22', '11', '0', '7', '3', '4', '12', '3') )
		self.growthRates_ = Utils.newFeGrowthRate( ('25', '20', '0', '15', '0', '10', '20', '5') )
		self.maxStats_ = Utils.newFeStatLine( ('70', '38', '25', '32', '25', '32', '40', '30') )
		self.promoted_ = True

class SkyKnight( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('16', '3', '0', '5', '7', '4', '2', '6') )
		self.growthRates_ = Utils.newFeGrowthRate( ('0', '10', '0', '10', '15', '20', '0', '20') )
		self.maxStats_ = Utils.newFeStatLine( ('35', '19', '16', '21', '23', '25', '18', '25') )
		self.promoted_ = False

class Apothecary( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('18', '6', '0', '4', '4', '2', '6', '2') )
		self.growthRates_ = Utils.newFeGrowthRate( ('20', '20', '0', '10', '10', '5', '10', '5') )
		self.maxStats_ = Utils.newFeStatLine( ('45', '24', '15', '19', '19', '21', '23', '20') )
		self.promoted_ = False

class BowKnight( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('18', '6', '0', '8', '9', '3', '5', '6') )
		self.growthRates_ = Utils.newFeGrowthRate( ('10', '10', '0', '15', '15', '10', '0', '10') )
		self.maxStats_ = Utils.newFeStatLine( ('55', '29', '25', '32', '33', '30', '27', '32') )
		self.promoted_ = True

class OniChieftain( FeClass ):
	def __init__( self ):
		self.baseStats_ = Utils.newFeStatLine( ('19', '9', '5', '2', '7', '0', '10', '5') )
		self.growthRates_ = Utils.newFeGrowthRate( ('10', '20', '15', '0', '10', '0', '20', '5') )
		self.maxStats_ = Utils.newFeStatLine( ('60', '34', '28', '25', '30', '25', '36', '31') )
		self.promoted_ = True

