from collections import namedtuple
import re

# GrowthRate: namedtuple of ints; StatLine: namedtuple of floats
stats = [ 'hp', 'Str', 'mag', 'skl', 'spd', 'lck', 'Def', 'res' ]
FeStatLine = namedtuple( "FeStatLine", stats )
FeGrowthRate = namedtuple( "FeGrowthRate", stats )

# newFeStatLine: FeStatLine factory that takes ensures float-type attributes
def newFeStatLine( statTuple ):
   assert len( statTuple ) == len( stats )
   # when you don't want to figure out how to manipulate tuples more elegantly...
   hp = float( statTuple[ 0 ] )
   Str = float( statTuple[ 1 ] )
   mag = float( statTuple[ 2 ] )
   skl = float( statTuple[ 3 ] )
   spd = float( statTuple[ 4 ] )
   lck = float( statTuple[ 5 ] )
   Def = float( statTuple[ 6 ] )
   res = float( statTuple[ 7 ] )
   return FeStatLine( hp, Str, mag, skl, spd, lck, Def, res )

# newFeGrowthRate: alias for FeGrowthRate, to keep with "new-" factory convention
def newFeGrowthRate( grTuple ):
   assert len( grTuple ) == len( stats )
   hp = int( grTuple[ 0 ] )
   Str = int( grTuple[ 1 ] )
   mag = int( grTuple[ 2 ] )
   skl = int( grTuple[ 3 ] )
   spd = int( grTuple[ 4 ] )
   lck = int( grTuple[ 5 ] )
   Def = int( grTuple[ 6 ] )
   res = int( grTuple[ 7 ] )
   return FeGrowthRate( hp, Str, mag, skl, spd, lck, Def, res )

# processClassName: explained inline
def processClassName( className ):
   puncRe = re.compile( r"[ -]|(?:\(ss\))" )
   retName = puncRe.sub( '', className )
   # The classes that are named different based on gender are dumb. This one especially.
   # For this case, set retName=Maid, then explicitly add the Butler-class with the same
   # info later.
   #
   # EDIT: okay so Monk and ShrineMaiden are dumb too
   if "," in retName:
      if retName == "Maid,Butler":
         retName = "Maid"
      elif retName == "Monk,ShrineMaiden":
         retName = "Monk"
   # Camelcase "Master of Arms"
   if retName == "MasterofArms":
      retName = "MasterOfArms"
   return retName

# classTranslate: map serenesforest class-notation to internal notation
def classTranslate( sfClass ):
   myClass = processClassName( sfClass )
   # Dumb thing where classes are named differently on different pages on serenesforest
   if myClass == "Trueblade":
      myClass = "Swordmaster"
   return myClass

# Need to determine in FeExtractStats whether class is promoted or not
promotedClass = [ "Adventurer", "Basara", "Berserker", "Blacksmith", "BowKnight", "Butler",
                  "DarkKnight", "FalconKnight", "General", "GreatKnight", "GreatMaster",
                  "Hero", "HoshidoNoble", "KinshiKnight", "Maid", "MaligKnight", "MasterNinja",
                  "MasterOfArms", "Mechanist", "Merchant", "NineTails", "NohrNoble",
                  "OniChieftain", "Onmyoji", "Paladin", "Sniper", "Sorcerer", "SpearMaster",
                  "Strategist", "Swordmaster", "Wolfssegner", "WyvernLord" ]

# This should be a DB item, but I don't want to do anymore html-parsing, so for now it's
# going here. Both dictionarys map stat-string to ( base stat, max stat, growth rate )-
# adjustment tuple
avatarBoon = { stats[ 0 ] : ( newFeStatLine( ( 3, 0, 0, 0, 0, 0, 0, 0 ) ),
                              newFeStatLine( ( 0, 1, 1, 0, 0, 2, 2, 2 ) ),
                              newFeGrowthRate( ( 15, 0, 0, 0, 0, 0, 5, 5 ) ) ),
               stats[ 1 ] : ( newFeStatLine( ( 0, 2, 0, 0, 0, 0, 0, 0 ) ),
                              newFeStatLine( ( 0, 4, 0, 2, 0, 0, 2, 0 ) ),
                              newFeGrowthRate( ( 0, 15, 0, 5, 0, 0, 5, 0 ) ) ),
               stats[ 2 ] : ( newFeStatLine( ( 0, 0, 3, 0, 0, 0, 0, 0 ) ),
                              newFeStatLine( ( 0, 4, 0, 0, 2, 0, 0, 2 ) ),
                              newFeGrowthRate( ( 0, 0, 20, 0, 5, 0, 0, 5 ) ) ),
               stats[ 3 ] : ( newFeStatLine( ( 0, 0, 0, 3, 0, 0, 0, 0 ) ),
                              newFeStatLine( ( 0, 2, 0, 4, 0, 0, 2, 0 ) ),
                              newFeGrowthRate( ( 0, 5, 0, 25, 0, 0, 5, 0 ) ) ),
               stats[ 4 ] : ( newFeStatLine( ( 0, 0, 0, 0, 2, 0, 0, 0 ) ),
                              newFeStatLine( ( 0, 0, 0, 2, 4, 2, 0, 0 ) ),
                              newFeGrowthRate( ( 0, 0, 0, 5, 15, 5, 0, 0 ) ) ),
               stats[ 5 ] : ( newFeStatLine( ( 0, 0, 0, 0, 0, 3, 0, 0 ) ),
                              newFeStatLine( ( 0, 2, 2, 0, 0, 4, 0, 0 ) ),
                              newFeGrowthRate( ( 0, 5, 5, 0, 0, 25, 0, 0 ) ) ),
               stats[ 6 ] : ( newFeStatLine( ( 0, 0, 0, 0, 0, 0, 1, 0 ) ),
                              newFeStatLine( ( 0, 0, 0, 0, 0, 2, 4, 2 ) ),
                              newFeGrowthRate( ( 0, 0, 0, 0, 0, 5, 10, 5 ) ) ),
               stats[ 7 ] : ( newFeStatLine( ( 0, 0, 0, 0, 0, 0, 0, 1 ) ),
                              newFeStatLine( ( 0, 0, 2, 0, 2, 0, 0, 4 ) ),
                              newFeGrowthRate( ( 0, 0, 5, 0, 5, 0, 0, 10 ) ) ) }
avatarBane = { stats[ 0 ] : ( newFeStatLine( ( -2, 0, 0, 0, 0, 0, 0, 0 ) ),
                              newFeStatLine( ( 0, -1, -1, 0, 0, -1, -1, -1 ) ),
                              newFeGrowthRate( ( -10, 0, 0, 0, 0, 0, -5, -5 ) ) ),
               stats[ 1 ] : ( newFeStatLine( ( 0, -1, 0, 0, 0, 0, 0, 0 ) ),
                              newFeStatLine( ( 0, -3, 0, -1, 0, 0, -1, 0 ) ),
                              newFeGrowthRate( ( 0, -10, 0, -5, 0, 0, -5, 0 ) ) ),
               stats[ 2 ] : ( newFeStatLine( ( 0, 0, -2, 0, 0, 0, 0, 0 ) ),
                              newFeStatLine( ( 0, 0, -3, 0, -1, 0, 0, -1 ) ),
                              newFeGrowthRate( ( 0, 0, -15, 0, -5, 0, 0, -5 ) ) ),
               stats[ 3 ] : ( newFeStatLine( ( 0, 0, 0, -2, 0, 0, 0, 0 ) ),
                              newFeStatLine( ( 0, -1, 0, -3, 0, 0, -1, 0 ) ),
                              newFeGrowthRate( ( 0, -5, 0, -20, 0, 0, -5, 0 ) ) ),
               stats[ 4 ] : ( newFeStatLine( ( 0, 0, 0, 0, -1, 0, 0, 0 ) ),
                              newFeStatLine( ( 0, 0, 0, -1, -3, -1, 0, 0 ) ),
                              newFeGrowthRate( ( 0, 0, 0, -5, -10, -5, 0, 0 ) ) ),
               stats[ 5 ] : ( newFeStatLine( ( 0, 0, 0, 0, 0, -2, 0, 0 ) ),
                              newFeStatLine( ( 0, -1, -1, 0, 0, -3, 0, 0 ) ),
                              newFeGrowthRate( ( 0, -5, -5, 0, 0, -20, 0, 0 ) ) ),
               stats[ 6 ] : ( newFeStatLine( ( 0, 0, 0, 0, 0, 0, -1, 0 ) ),
                              newFeStatLine( ( 0, 0, 0, 0, 0, -1, -3, -1 ) ),
                              newFeGrowthRate( ( 0, 0, 0, 0, 0, -5, -10, -5 ) ) ),
               stats[ 7 ] : ( newFeStatLine( ( 0, 0, 0, 0, 0, 0, 0, -1 ) ),
                              newFeStatLine( ( 0, 0, -1, 0, -1, 0, 0, -3 ) ),
                              newFeGrowthRate( ( 0, 0, -5, 0, -5, 0, 0, -10 ) ) ) }

# The two parent classes are quite similar. Maybe FeCharacter should have inherited from
# FeClass. Oh well.
charParentClass = \
   "class FeCharacter( object ):\n\tdef __init__( self ):\n\t\tself.level_ = None\n\t\tself.baseClass_ = None\n\t\tself.baseStats_ = None\n\t\tself.growthRates_ = None\n\t\tself.maxStats_ = None\n\tdef baseLevel( self ):\n\t\treturn self.level_\n\tdef baseClass( self ):\n\t\treturn self.baseClass_\n\tdef baseStats( self ):\n\t\treturn self.baseStats_\n\tdef growthRates( self ):\n\t\treturn self.growthRates_\n\tdef maxStats( self ):\n\t\treturn self.maxStats_\n\n"

classParentClass = \
   "class FeClass( object ):\n\tdef __init__( self ):\n\t\tself.baseStats_ = None\n\t\tself.growthRates_ = None\n\t\tself.maxStats_ = None\n\t\tself.promoted_ = False\n\tdef baseStats( self ):\n\t\treturn self.baseStats_\n\tdef growthRates( self ):\n\t\treturn self.growthRates_\n\tdef maxStats( self ):\n\t\treturn self.maxStats_\n\tdef promoted( self ):\n\t\treturn self.promoted_\n\n"

baseStatInitializer = \
   "class %s( FeCharacter ):\n\tdef __init__( self ):\n\t\tself.level_ = %s\n\t\tself.baseClass_ = ClassDb.%s()\n\t\tself.baseStats_ = Utils.newFeStatLine( %s )\n"

classBaseStatInitializer = \
   "class %s( FeClass ):\n\tdef __init__( self ):\n\t\tself.baseStats_ = Utils.newFeStatLine( %s )\n"

growthRateInitializer = \
   "\t\tself.growthRates_ = Utils.newFeGrowthRate( %s )\n"

maxStatInitializer = \
   "\t\tself.maxStats_ = Utils.newFeStatLine( %s )\n"

promotedInitializer = \
   "\t\tself.promoted_ = %s\n"
